﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;

namespace AggiornaFatturaElettronica
{
    class Program
    {
        private string BaseUrlLog;
        private string GeneraLog = "false";
        private SAPbobsCOM.Documents oOINV;
        private SAPbobsCOM.Documents oORIN;
        XmlDocument docXml = new XmlDocument();
        private SAPbobsCOM.Company oCompany;
        private SAPbobsCOM.Recordset oRecordset;
        private SAPbobsCOM.Recordset oRecordset1;
        private SAPbobsCOM.SBObob oSBObob;

        static void Main(string[] args)
        {
            var prog = new Program();
            prog.elabora();

        }

        public void elabora()
        {
            docXml.Load("parametriDB.xml");
            connectionSAP(docXml);
            oRecordset1 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);


            //ELABORO TUTTE LE FATT DI VENDITA
            string Query = "";

            Query += "SELECT T0.\"DocEntry\", T0.\"DocNum\", T0.\"NumAtCard\", T0.\"U_FO_EC_NEGOZIO\" FROM OINV T0 LEFT JOIN ECM2 T1 ON T0.\"DocEntry\" = T1.\"SrcObjAbs\" ";
            Query += " AND T1.\"SrcObjType\" = 13 WHERE T1.\"ActStatus\" IS NULL AND T0.\"U_FO_EC_GENDOCELET\" = 'Y' ";
            oRecordset1.DoQuery(Query);
            while (!oRecordset1.EoF)
            {
                int DocEntry = int.Parse(oRecordset1.Fields.Item(0).Value.ToString());
                int DocNum = int.Parse(oRecordset1.Fields.Item(1).Value.ToString());
                string NumAtCard = oRecordset1.Fields.Item(2).Value.ToString();
                string Negozio = oRecordset1.Fields.Item(3).Value.ToString();
                int DocType = 13;
                string Res = AggiornaFattura(DocEntry, DocNum, NumAtCard, Negozio, DocType);
                string Testo = "";
                if (Res == "0 - ")
                {
                    Testo = "Elaborazione Fattura Numero " + DocNum + " con successo " + Res;
                }
                else
                {
                    Testo = "Elaborazione Fattura Numero " + DocNum + " con errore: " + Res;
                }

                LogGenerale(Testo);
                oRecordset1.MoveNext();
            }

            //ELABORO TUTTE LE NC DI VENDITA
            Query = "";
            Query += "SELECT T0.\"DocEntry\", T0.\"DocNum\", T0.\"NumAtCard\", T0.\"U_FO_EC_NEGOZIO\" FROM ORIN T0 LEFT JOIN ECM2 T1 ON T0.\"DocEntry\" = T1.\"SrcObjAbs\" ";
            Query += " AND T1.\"SrcObjType\" = 14 WHERE T1.\"ActStatus\" IS NULL AND T0.\"U_FO_EC_GENDOCELET\" = 'Y' ";
            oRecordset1.DoQuery(Query);
            while (!oRecordset1.EoF)
            {
                int DocEntry = int.Parse(oRecordset1.Fields.Item(0).Value.ToString());
                int DocNum = int.Parse(oRecordset1.Fields.Item(1).Value.ToString());
                string NumAtCard = oRecordset1.Fields.Item(2).Value.ToString();
                string Negozio = oRecordset1.Fields.Item(3).Value.ToString();
                int DocType = 14;
                string Res = AggiornaNotadiCredito(DocEntry, DocNum, NumAtCard, Negozio, DocType);
                string Testo = "";
                if (Res == "0 - ")
                {
                    Testo = "Elaborazione Nota di Credito Numero " + DocNum + " con successo " + Res;
                }
                else
                {
                    Testo = "Elaborazione Nota di Credito Numero " + DocNum + " con errore: " + Res;
                }
                LogGenerale(Testo);
                oRecordset1.MoveNext();
            }

            //invio un feed positivo al monitor interno
            oRecordset.DoQuery(" Select \"Name\" as \"U_IDSOCIETA\" from \"@ADDONPAR\" where \"Code\" = 'FO_IDSOCIETA' ");
            MonitorSendFeed.Sender.SendFeedToMonitor(oRecordset.Fields.Item(0).Value.ToString(), "8", 1, "");
            
            disconnectionSAP();
        }

        public string AggiornaFattura(int DocEntry, int DocNum, string NumAtCard, string Negozio, int DocType)
        {
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            int MappingId = GetMappingID(oRecordset, Negozio);
            string err = "";
            int code = 0;
            if (MappingId != 0 )
            {
                oOINV = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                oOINV.GetByKey(DocEntry);
                oOINV.ElectronicProtocols.ProtocolCode = SAPbobsCOM.ElectronicDocProtocolCodeEnum.edpc_FPA;
                oOINV.ElectronicProtocols.GenerationType = SAPbobsCOM.ElectronicDocGenTypeEnum.edgt_Generate;
                oOINV.ElectronicProtocols.MappingID = MappingId;
                oOINV.UserFields.Fields.Item("U_FO_EC_GENDOCELET").Value = "N";
                
                int res = oOINV.Update();
                if (res != 0)
                {

                    oCompany.GetLastError(out code, out err);
                    string UpQuery = "";
                    string Data = DateTime.Now.ToString("yyyy-MM-dd");
                    string Ora = DateTime.Now.ToString("HHmm");
                    if (oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    {
                        UpQuery += "INSERT INTO \"@FO_EC_ERRDOCELET\" ( \"Code\", \"U_FO_EC_DATA\", \"U_FO_EC_ORA\", \"U_FO_EC_NUMERO\", \"U_FO_EC_NEGOZIO\", \"U_FO_EC_DESCERRORE\", \"U_FO_EC_DOCTYPE\", \"U_FO_EC_DOCENTRY\") ";
                        UpQuery += " VALUES (\"@FO_EC_ERRDOCELET_S\".nextval,'" + Data + "','" + Ora + "','" + NumAtCard + "','" + Negozio + "','" + err + "','13','" + DocEntry + "')";
                    }
                    else
                    {//SQL
                        UpQuery += "INSERT INTO \"@FO_EC_ERRDOCELET\" (  \"U_FO_EC_DATA\", \"U_FO_EC_ORA\", \"U_FO_EC_NUMERO\", \"U_FO_EC_NEGOZIO\", \"U_FO_EC_DESCERRORE\", \"U_FO_EC_DOCTYPE\", \"U_FO_EC_DOCENTRY\") ";
                        UpQuery += " VALUES ('" + Data + "','" + Ora + "','" + NumAtCard + "','" + Negozio + "','" + err + "','13','" + DocEntry + "')";
                    }
                    oRecordset.DoQuery(UpQuery);

                    string UpDoc = "UPDATE OINV SET \"U_FO_EC_GENDOCELET\" = 'E' WHERE \"DocEntry\" = '" + DocEntry + "'";
                    oRecordset.DoQuery(UpDoc);
                }
            }
            else
            {
                code = -9999;
                err = " Mapping ID non trovato contollare parametrizzazione ADDONPAR";
            }
            

            return code + " - " + err;
        }

        public string AggiornaNotadiCredito(int DocEntry, int DocNum, string NumAtCard, string Negozio, int DocType)
        {
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            int MappingId = GetMappingID(oRecordset, Negozio);
            string err = "";
            int code = 0;
            if (MappingId != 0)
            {
                oORIN = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
                oORIN.GetByKey(DocEntry);
                oORIN.ElectronicProtocols.ProtocolCode = SAPbobsCOM.ElectronicDocProtocolCodeEnum.edpc_FPA;
                oORIN.ElectronicProtocols.GenerationType = SAPbobsCOM.ElectronicDocGenTypeEnum.edgt_Generate;
                oORIN.ElectronicProtocols.MappingID = MappingId;
                oORIN.UserFields.Fields.Item("U_FO_EC_GENDOCELET").Value = "N";
                int res = oORIN.Update();
                if (res != 0)
                {
                    oCompany.GetLastError(out code, out err);
                    string UpQuery = "";
                    string Data = DateTime.Now.ToString("yyyy-MM-dd");
                    string Ora = DateTime.Now.ToString("HHmm");

                    if (oCompany.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                    {
                        UpQuery += "INSERT INTO \"@FO_EC_ERRDOCELET\" ( \"Code\", \"U_FO_EC_DATA\", \"U_FO_EC_ORA\", \"U_FO_EC_NUMERO\", \"U_FO_EC_NEGOZIO\", \"U_FO_EC_DESCERRORE\", \"U_FO_EC_DOCTYPE\", \"U_FO_EC_DOCENTRY\") ";
                        UpQuery += " VALUES (\"@FO_EC_ERRDOCELET_S\".nextval,'" + Data + "','" + Ora + "','" + NumAtCard + "','" + Negozio + "','" + err + "','14','" + DocEntry + "')";
                    }
                    else
                    { //SQL
                        UpQuery += "INSERT INTO \"@FO_EC_ERRDOCELET\" (  \"U_FO_EC_DATA\", \"U_FO_EC_ORA\", \"U_FO_EC_NUMERO\", \"U_FO_EC_NEGOZIO\", \"U_FO_EC_DESCERRORE\", \"U_FO_EC_DOCTYPE\", \"U_FO_EC_DOCENTRY\") ";
                        UpQuery += " VALUES ('" + Data + "','" + Ora + "','" + NumAtCard + "','" + Negozio + "','" + err + "','14','" + DocEntry + "')";
                    }
                    oRecordset.DoQuery(UpQuery);

                    string UpDoc = "UPDATE ORIN SET \"U_FO_EC_GENDOCELET\" = 'E' WHERE \"DocEntry\" = '" + DocEntry + "'";
                    oRecordset.DoQuery(UpDoc);
                }
            }
            else
            {
                code = -9999;
                err = " Mapping ID non trovato contollare parametrizzazione ADDONPAR";
            }
            return code + " - " + err;
        }

        public void connectionSAP(XmlDocument docXml)
        {
            oCompany = new SAPbobsCOM.Company();

            //Leggo i parametri di connessione dal file xml
            XmlNodeList elemList = docXml.GetElementsByTagName("DB");
            foreach (XmlNode node in elemList)
            {
                BaseUrlLog = node.Attributes["PathLog"].Value;
                GeneraLog = node.Attributes["GeneraLog"].Value;

                oCompany.Server = node.Attributes["Server"].Value;
                //oCompany.LicenseServer = node.Attributes["License"].Value;
                //oCompany.DbUserName = node.Attributes["DbUserName"].Value;
                //oCompany.DbPassword = node.Attributes["DbPassword"].Value;
                oCompany.CompanyDB = node.Attributes["CompanyDB"].Value;
                oCompany.UserName = node.Attributes["UserName"].Value;
                oCompany.Password = node.Attributes["Password"].Value;
                /*
                Console.WriteLine("Server: " + node.Attributes["Server"].Value);
                Console.WriteLine("DbUserName: " + node.Attributes["DbUserName"].Value);
                Console.WriteLine("DbPassword: " + node.Attributes["DbPassword"].Value);
                Console.WriteLine("CompanyDB: " + node.Attributes["CompanyDB"].Value);
                Console.WriteLine("Password: " + node.Attributes["Password"].Value);
                */
                switch (node.Attributes["DBType"].Value)
                {
                    case "SQL19":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2019;
                        break;

                    case "SQL17":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                        break;

                    case "SQL16":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                        break;

                    case "SQL14":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                        break;

                    case "SQL12":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                        break;

                    case "HANA":
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                        break;

                    default:
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                        break;
                }
            }

            oCompany.UseTrusted = false;


            int errCode = oCompany.Connect();

            if (errCode != 0)
            {
                string err;
                int code;
                oCompany.GetLastError(out code, out err);
                Console.WriteLine("Err Connessione sap : " + code + " " + err);
                return;
            }
            oSBObob = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
        }

        public void disconnectionSAP()
        {
            oCompany.Disconnect();
            oCompany = null;
        }


        public void LogGenerale(string TestoIn)
        {
            if (GeneraLog == "true")
            {
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string logFile = BaseUrlLog + "/LOG_DOCELETT_" + dt.ToString("yyyyMMdd") + ".txt";

                using (StreamWriter w = File.AppendText(logFile))
                {
                    w.WriteLine(TestoIn);
                }
            }
            Console.WriteLine(TestoIn);
        }

        public int GetMappingID(SAPbobsCOM.Recordset oRecordset, string Negozio)
        {
            //Recupero il tipo di documento elettronico
            oRecordset.DoQuery("SELECT \"U_FO_EC_GESTIONE_PREZZI\" FROM \"@FO_EC_DETNEGOZIO\" WHERE \"U_FO_EC_CARDCODE\" = '" + Negozio + "'");
            int MappingID = 0;
            if (oRecordset.Fields.Item(0).Value.ToString() == "N")
            {
                oRecordset.DoQuery("SELECT \"Name\" FROM \"@ADDONPAR\" WHERE \"Code\" = 'FILE_ELETT_OSS'");
                MappingID = int.Parse(oRecordset.Fields.Item(0).Value.ToString());
            }
            else
            {
                oRecordset.DoQuery("SELECT \"Name\" FROM \"@ADDONPAR\" WHERE \"Code\" = 'FILE_ELETT_STANDARD'");
                MappingID = int.Parse(oRecordset.Fields.Item(0).Value.ToString());
            }

            return MappingID;
        }
    }
}
